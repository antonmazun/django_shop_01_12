from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .models import Book
from .common import calc_object
from .forms import BookForm


# Create your views here.


def home(request):
    all_books = Book.objects.all()
    total_sum = 0
    for book in all_books:
        total_sum += book.get_new_price()
    # for book in all_books:
    #     print("title " , book.title)
    #     print("description " , book.description)
    #     print('*' * 15)
    #
    # print('!!!')
    return render(request, 'home_page.html', {
        'books': all_books,
        'total_sum': total_sum,
        'active_tab': 'home'
    })


def detail_book(request, book_id):
    current_book = Book.objects.get(id=book_id)
    print('current_book ', current_book.get_new_price())
    return render(request, 'detail_book.html', {
        'current_book': current_book
    })


def examples(request):
    ctx = {
        "operations": calc_object.keys(),
        'active_tab': 'examples'
    }
    if request.method == 'GET':
        result = None
        try:
            first_number = float(request.GET.get('first_number'))
            operation = request.GET.get('operation')
            second_number = float(request.GET.get('second_number'))
            result = calc_object[operation](first_number, second_number)
            ctx['result'] = result
            print(first_number, operation, second_number)
        except (ZeroDivisionError, ValueError) as e:
            ctx['error'] = str(e)

        except KeyError as e:
            ctx['error'] = 'Invalid operation'
        except Exception as e:
            print(type(e), e)

        else:
            ctx['params'] = {
                'first_number': first_number,
                'operation': operation,
                'second_number': second_number,
            }
    return render(request, 'examples.html', ctx)


def about_us(req):
    return render(req, 'about.html', {})


def create_book(req):
    context = {
        'active_tab': 'create_book',
        'action': '/create-book/',
        'mode': 'new'
    }
    if req.method == 'GET':
        form = BookForm()
        context.update(form=form)
    elif req.method == 'POST':
        form = BookForm(req.POST)
        if form.is_valid():
            print(form.cleaned_data)
            form.save()
            context.update({
                'form': BookForm(),
                'msg': "Book saved."
            })
        else:
            print(form.errors)
            context.update(form=form)
    return render(req, 'book/create_book.html', context)


def manage_books(request):
    context = {
        'active_tab': 'manage_books',
    }
    books = Book.objects.all().order_by('-id')
    context.update(books=books)
    return render(request, 'book/manage_books.html', context)


def update_book(request, pk):
    context = {
        'action': '/update-book/{}'.format(pk),
        'mode': 'edit'
    }
    if request.method == 'GET':
        form = BookForm(instance=Book.objects.get(id=pk))
        context['form'] = form
    elif request.method == 'POST':
        form = BookForm(request.POST, instance=Book.objects.get(id=pk))
        print('form ', form)
        if form.is_valid():
            form.save()
            context['msg'] = "Book updated."
    return render(request, 'book/create_book.html', context)


def delete_book(request, pk):
    try:
        target_book = Book.objects.get(id=pk)
        target_book.delete()
    except Exception as e:
        print(type(e), e)
        return redirect('/manage-books/')
    else:
        return redirect('/manage-books/')


import random


def generate_books(request):
    random_text = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque, quaerat.'.split()
    if request.method == 'POST':
        count = int(request.POST.get('count'))
        for _ in range(count):
            is_sale = random.choice([True, False])
            payload_data = {
                'title': ' '.join(random.sample(random_text, 2)),
                'description': ' '.join(random.sample(random_text, 5)) * 10,
                'price': random.randrange(10, 150),
                'is_sale': is_sale,
                'percent_sale': random.randint(1, 99) if is_sale else 0,
                'genre': random.sample([x for x, y in Book.GENRE], 1)[0]
            }
            print('payload_data ->>', payload_data)
            Book.objects.create(**payload_data)
        print('POST!', count)
        return redirect('/manage-books/')


def ajax_delete_book(request):
    if request.is_ajax():
        book_id = int(request.GET.get('id'))
        Book.objects.get(id=book_id).delete()
        print('AJAX REQUEST!!!', book_id)
        return JsonResponse({
            'status': True
        })
