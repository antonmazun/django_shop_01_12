from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.home),
    path('book-detail/<int:book_id>' , views.detail_book),
    path('examples/' , views.examples),
    path('about-us/' , views.about_us),
    path('create-book/' , views.create_book),
    path('manage-books/' , views.manage_books),
    path('update-book/<int:pk>' , views.update_book),
    path('delete-book/<int:pk>' , views.delete_book),
    path('ajax-delete-book/' , views.ajax_delete_book),
    path('generate-books/' , views.generate_books),
]
