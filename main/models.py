from django.db import models

# Create your models here.

"""
Book
    - title (str)
    - description (str)
    - price (float)
    - is_sale (bool)
    - percent_sale (float)
    - genre (str)

"""


class Book(models.Model):
    GENRE = (
        ('drama', 'Драма'),
        ('comedy', 'Комедия')
    )
    title = models.CharField(max_length=255)
    description = models.TextField(max_length=2000)
    price = models.FloatField()
    is_sale = models.BooleanField(default=False)
    percent_sale = models.FloatField(blank=True, null=True)
    genre = models.CharField(choices=GENRE, max_length=50)

    def __str__(self):
        return '{} {}'.format(self.title,
                              self.description[:10] + "..." if len(self.description) > 10 else self.description)

    def get_new_price(self):
        if self.is_sale:
            return self.price * ((100 - self.percent_sale) / 100)
        else:
            return self.price
