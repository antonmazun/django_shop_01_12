def add(a, b):
    return a + b


def sub(a, b):
    return a - b


def div(a, b):
    return a / b


def mul(a, b):
    return a * b


def int_div(a, b):
    return a // b


calc_object = {
    '+': add,
    '-': sub,
    '*': mul,
    '/': div,
    '//': int_div
}
